/* Copyright 2018-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by
 * applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */
package com.github.drinkjava2.gosqlgo;

import com.github.drinkjava2.gosqlgo.util.GsgStrUtils;

/**
 * The SrcBuilder will build child class source code based on given template
 * class
 * 
 * @author Yong Zhu
 * @since 1.0.0
 */
public abstract class SrcBuilder {

	public abstract ServletTemplate getTemplate();

	/** Based on front text and template to create child class java source code */
	public abstract String createSourceCode(String className, String frontText);

	/** Based on SqlJavaPiece and template to create child class java source code */
	public abstract String createSourceCode(String className, SqlJavaPiece piece);

	/** Based on SqlJavaPiece and template to create front text */
	public abstract String createFrontText(SqlJavaPiece piece);

	/** Build GSG TAGS for java source code */
	public static String buildGsgTagsForJavaSourceCode(SqlJavaPiece piece) {
		StringBuilder sb = new StringBuilder();
		if (piece.isTx())
			sb.append("\n		 tx = true;// GSG TX");
		if (piece.isServ())
			sb.append("\n		 serv = true;// GSG SERV");
		if (piece.isFront())
			sb.append("\n		 front = true;// GSG FRONT");
		if (!GsgStrUtils.isEmpty(piece.getId()))
			sb.append("\n		 id = \"").append(GsgStrUtils.replaceFirst(piece.getId(), "#", ""))
					.append("\";// GSG ID");
		return sb.toString();
	}

	/** build Front Leading Tags */
	public static String buildFrontLeadingTagsAndImports(SqlJavaPiece piece) {
		StringBuilder sb = new StringBuilder();
		if (piece.isTx())
			sb.append("TX ");
		if (piece.isServ())
			sb.append("SERV ");
		if (piece.isFront())
			sb.append("FRONT ");
		if (!GsgStrUtils.isEmpty(piece.getId()))
			sb.append("#").append(piece.getId()).append(" ");
		if (!GsgStrUtils.isEmpty(piece.getImports()))
			sb.append(piece.getImports()).append(" "); 
		return sb.toString();
	}

}
