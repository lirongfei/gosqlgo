## gsg-tomcat  
这是一个GoSqlGo在Tomcat环境下运行的示范项目。  
  
编译及运行本项目需Java8或以上。   
 
## 三种运行方式:    

#### 方式1，发布war包到本机的Tomcat7或Tomcat8目录下执行：   
运行：修改run-normal-tomcat.bat批处理文件中的TomcatFolder为本机Tomcat目录，并执行        
查看：浏览器输入 http://localhost  

#### 方式2, 命令行方式运行嵌入式Tomcat，这种方式本机不需要安装Tomcat, Maven会自动下载      
运行：双击运行run-embeded-tomcat.bat批处理即可     
查看：浏览器输入 http://localhost  

#### 方式3, 导入到Eclipse中运行  
1. 运行run-embeded-tomcat.bat批处理一次  
2. 运行maven_eclipse_eclipse.bat批处理，生成eclipse配置  
3. 打开Eclipse,导入项目,并运行其中的EmbedMain.java的main方法  
查看：浏览器输入 http://localhost   

## 开发阶段和布署阶段

#### 开发阶段：  
确保在配置文件src\main\resources\GoSqlGo.properties中有以下行:  
stage=develop
即可在HTML/Javascript中混合书写SQL和Java代码了，有两个Javascript方法$qry和$java方法分别对应两种用法。  
   
#### 布署阶段 
1. 确保在配置文件src\main\resources\GoSqlGo.properties中有以下行:  
stage=product  
这个选项设定为product后，将不再动态编译客户端发来的SQL和Java代码，只允许运行已抽取到服务端的代码。

2. 抽取方法是：将项目导入IDE后，在任意位置运行以下代码，即可将所有HTML/Javascript中的SQL和Java代码抽取到服务端，即本示例项目的src\main\java\com\demo\deploy目录。
```
new DeployTool().toServ(); 
```

#### 逆操作
已经抽取到服务端的SQL和代码片段，IDE中运行以下代码即可逆向回原到HTML/Javascript文件中去。  
```
new DeployTool().toFront();
```
注意如果代码片段开头有SERV控制字，一旦被抽取到服务端，将不会被回原到HTML/Javascript文件中。


#### 附：关键字说明:
TX			打开事务  
FRONT		一直保留在前端，除非用deploy()命令才能强制抽取到后端  
SERV		一旦抽到到后端，就一直留在后端  
#xxxxx形式	以#号开头，表示手工定义抽取到服务端后的Java类名，如果不定义，类名由工具随机生成  
import		引入Java包  

使用示例： qry('TX SERV #ReadAmount import abc.Demo1;import abc.Demo2;  select amount from account where id=? and amount<>?', 'A',0);   
注意上面$qry和$java方法里第一个参数的单引号不是普通的单引号，而是ESC键下方的那个，它在Javascript中支持多行文本。当布署阶段时抽取成功后将改为普通单引号形式，如$qry('AccountTransfer')。  


