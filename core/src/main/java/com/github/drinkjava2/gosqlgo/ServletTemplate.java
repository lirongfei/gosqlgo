/* Copyright 2018-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by
 * applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */
package com.github.drinkjava2.gosqlgo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

import com.github.drinkjava2.jwebbox.WebBox;

/**
 * This is the base environment store servlet environment info. currently
 * version GoSqlGO depends on JSP .
 * 
 * @author Yong Zhu
 * @since 1.0.0
 */
@SuppressWarnings("all")
public abstract class ServletTemplate {
	protected String $0;
	protected String $1;
	protected String $2;
	protected String $3;
	protected String $4;
	protected String $5;
	protected String $6;
	protected String $7;
	protected String $8;
	protected String $9;
	protected String $10;
	protected String $11;
	protected String $12;
	protected String $13;
	protected String $14;
	protected String $15;
	protected String $16;
	protected String $17;
	protected String $18;
	protected String $19;
	protected String $20;

	protected PageContext pageContext; // JSP PageContext instance

	protected Boolean tx = false; // open transaction for method call

	protected String id = null; // null: id is random, "#xxxxxxx":assign ID

	protected Boolean serv = false; // sql or java piece should always stay at server side

	protected Boolean front = false; // sql or java piece should always stay at front, except use deploy command

	protected String imports = ""; // import abc.def; import abc.hij;...

	protected String body = ""; // the SQL or Java piece body text

	public HttpSession getSession() {
		return pageContext.getSession();
	}

	public HttpServletRequest getRequest() {
		return (HttpServletRequest) pageContext.getRequest();
	}

	public HttpServletResponse getResponse() {
		return (HttpServletResponse) pageContext.getResponse();
	}

	public Map<String, String[]> getParameterMap() {
		return getRequest().getParameterMap();
	}

	public String getParam(String paramkey) {
		return getRequest().getParameter(paramkey);
	}

	public String[] getParamArray() {
		List<String> paramList = new ArrayList<String>();
		for (int i = 1; i <= 100; i++) {
			String parameter = getParam("$" + i);
			if (parameter != null)
				paramList.add(parameter);
			else
				break;
		}
		return paramList.toArray(new String[paramList.size()]);
	}

	public void initParams() {
		$0 = getParam("$0");
		$1 = getParam("$1");
		$2 = getParam("$2");
		$3 = getParam("$3");
		$4 = getParam("$4");
		$5 = getParam("$5");
		$6 = getParam("$6");
		$7 = getParam("$7");
		$8 = getParam("$8");
		$9 = getParam("$9");
		$10 = getParam("$10");
		$11 = getParam("$11");
		$12 = getParam("$12");
		$13 = getParam("$13");
		$14 = getParam("$14");
		$15 = getParam("$15");
		$16 = getParam("$16");
		$17 = getParam("$17");
		$18 = getParam("$18");
		$19 = getParam("$19");
		$20 = getParam("$20");
	}

	public abstract Object execute();

	public abstract WebBox run();

	// getter & setters ==========
	public PageContext getPageContext() {
		return pageContext;
	}

	public void setPageContext(PageContext pageContext) {
		this.pageContext = pageContext;
	}

	public Boolean getTx() {
		return tx;
	}

	public void setTx(Boolean tx) {
		this.tx = tx;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getServ() {
		return serv;
	}

	public void setServ(Boolean serv) {
		this.serv = serv;
	}

	public Boolean getFront() {
		return front;
	}

	public void setFront(Boolean front) {
		this.front = front;
	}

	public String getImports() {
		return imports;
	}

	public void setImports(String imports) {
		this.imports = imports;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
