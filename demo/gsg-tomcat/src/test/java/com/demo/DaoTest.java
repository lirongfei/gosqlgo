/* Copyright 2018-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by
 * applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */
package com.demo;

import org.junit.Assert;
import org.junit.Test;

import com.demo.Dispatch;
import com.demo.InitConfig;
import com.demo.jspmock.MockPageContext;
import com.demo.jspmock.MockRequest;
import com.github.drinkjava2.gosqlgo.SqlJavaPiece;
import com.github.drinkjava2.jwebbox.WebBox;

import text.Texts.JavaPiece;

/**
 * Unit test of Dao
 * 
 * @author Yong Zhu
 * @since 1.0.0
 */
public class DaoTest {

	@Test
	public void testParse() {
		String java = new JavaPiece().toString();
		SqlJavaPiece piece = SqlJavaPiece.parseFromFrontText(java);
		Assert.assertEquals(true, piece.isTx());
		Assert.assertEquals(true, piece.isServ());
		Assert.assertEquals(true, piece.isFront());
		Assert.assertEquals("#GetAmount", piece.getId());
		System.out.println( piece.getImports());
		Assert.assertEquals(" import java.lang.Object; // GSG IMPORT\n", piece.getImports());
	}

	@Test
	public void testQry() {
		InitConfig.initDataBase();
		MockPageContext pageContext = new MockPageContext();
		MockRequest request = (MockRequest) pageContext.getRequest();
		request.setRequestURI("/qry.htm");
		request.setParameter("$0",
				"TX SERV #234 import java.lang.Object; import java.lang.String;  select amount from account where id=? and amount>=?");
		request.setParameter("$1", "A");
		request.setParameter("$2", "0");
		WebBox box = Dispatch.doDispatch(pageContext);
		Assert.assertEquals("700", box.getText());

		request.setParameter("$1", "B");
		box = Dispatch.doDispatch(pageContext);
		Assert.assertEquals("300", box.getText());
	}

	@Test
	public void testJava() {
		InitConfig.initDataBase();
		MockPageContext pageContext = new MockPageContext();
		MockRequest request = (MockRequest) pageContext.getRequest();
		request.setRequestURI("/java.htm");
		request.setParameter("$0", "" + new JavaPiece());

		request.setParameter("$1", "A");
		request.setParameter("$2", "B");
		request.setParameter("$3", "100");
		WebBox box = Dispatch.doDispatch(pageContext);
		Assert.assertEquals("Transfer Success!|600|400", box.getText());
	}

}
