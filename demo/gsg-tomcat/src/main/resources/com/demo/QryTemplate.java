package com.demo;

import static com.github.drinkjava2.jsqlbox.JSQLBOX.*;
import com.demo.entity.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import com.demo.page.Pages.*;  

// GSG IMPORTS

@SuppressWarnings("all")
public class QryTemplate extends DaoTemplate {
	{ // GSG TAGS
	}

	@Override
	public Object execute() {
		/* GSG SQL BEGIN */
		String sql = null;
		/* GSG SQL END */
		String[] paramArray = getParamArray();
		if (paramArray.length == 0)
			return nQueryForObject(sql);
		else
			return pQueryForObject(sql, paramArray);
	}

}
